export const USER_ROLE = 'user';
export const ADMIN_ROLE = 'admin';

export const PRIVILEGE = {
  [USER_ROLE]: 1,
  [ADMIN_ROLE]: 4,
};

export default class Privilege {
  constructor() {
    this.role = USER_ROLE;
    this.level = PRIVILEGE[USER_ROLE];
  }
}
