import React from 'react';
import AmbientFilter from './Filters/AmbientFilter';

const Overlay = props => (
  <div>
    <AmbientFilter />
  </div>
);

export default Overlay;
