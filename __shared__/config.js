const config = {
  animationsType: 'animations',
  tilesetsType: 'tilesets',
  texturePath: 'assets/graphics',
  audioPath: 'assets/audio',
  initPath: 'assets/init',
  mapPath: 'assets/maps',
};

module.exports = config;
