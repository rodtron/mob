import { ADMIN_ROLE, USER_ROLE } from 'ecs/components/character/Privilege';
import s from './character.styles.json';
export const styles = s;

export const CHARACTER_ROLE_STYLES = {
  [USER_ROLE]: { ...styles.name, fill: styles.privilegeColors[USER_ROLE] },
  [ADMIN_ROLE]: { ...styles.name, fill: styles.privilegeColors[ADMIN_ROLE] }
};

export default null;
