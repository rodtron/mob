import window from './window.json';
export const canvasWidth = window.width;
export const canvasHeight = window.height;

export default {
  canvasWidth,
  canvasHeight,
  window
};
