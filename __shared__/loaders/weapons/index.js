import { HEADINGS, EAST, NORTH, SOUTH, WEST } from 'shared/constants/game/Game';
import { parseDirectionAnimationByModel, transform } from 'shared/loaders/util';
import Weapon from 'shared/models/data/character/Weapon';

/**
 * Parses JSON weapons file into a key-value map
 * of weapon id's and their respective `Weapon`
 * models
 * @param data
 * @param animations
 * @returns {MapObject.<string, Weapon>}
 */
export const getWeapons = (data, animations) => {
  // const order = [EAST, SOUTH, NORTH, WEST].map(h => HEADINGS[h]);
  return transform(data, parseDirectionAnimationByModel(animations, Weapon));
};

export default getWeapons;
