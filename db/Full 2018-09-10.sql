-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 10, 2018 at 06:01 PM
-- Server version: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 7.2.7-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mob`
--

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `attribute_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`attribute_id`, `name`) VALUES
(1, 'strength'),
(2, 'agility'),
(3, 'inteligence'),
(4, 'charisma'),
(5, 'constitution');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `class_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`class_id`, `name`) VALUES
(1, 'mage'),
(2, 'cleric'),
(3, 'warrior'),
(4, 'assasin'),
(5, 'thief'),
(6, 'bard'),
(7, 'druid'),
(8, 'bandit'),
(9, 'paladin'),
(10, 'hunter'),
(11, 'worker'),
(12, 'pirate');

-- --------------------------------------------------------

--
-- Table structure for table `constants`
--

CREATE TABLE `constants` (
  `constant_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `constants`
--

INSERT INTO `constants` (`constant_id`, `name`, `value`) VALUES
(1, 'bloodFX', '14'),
(2, 'newbieLimit', '12');

-- --------------------------------------------------------

--
-- Table structure for table `objects`
--

CREATE TABLE `objects` (
  `object_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(511) NOT NULL,
  `type_id` int(11) NOT NULL,
  `grh` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `object_attributes`
--

CREATE TABLE `object_attributes` (
  `object_id` int(11) NOT NULL,
  `attribute` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `object_types`
--

CREATE TABLE `object_types` (
  `type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `object_types`
--

INSERT INTO `object_types` (`type_id`, `name`) VALUES
(1, 'food'),
(2, 'weapon'),
(3, 'armor'),
(4, 'tree'),
(5, 'money'),
(6, 'door'),
(7, 'container'),
(8, 'sign'),
(9, 'key'),
(10, 'forum'),
(11, 'potion'),
(12, 'book'),
(13, 'drink'),
(14, 'wood'),
(15, 'bonfire'),
(16, 'shield'),
(17, 'helmet'),
(18, 'ring'),
(19, 'teleport'),
(20, 'furniture'),
(21, 'jewel'),
(22, 'mine'),
(23, 'ore'),
(24, 'scroll'),
(25, 'aura'),
(26, 'instrument'),
(27, 'anvil'),
(28, 'forge'),
(29, 'gem'),
(30, 'flower'),
(31, 'boat'),
(32, 'arrow'),
(33, 'emptyBottle'),
(34, 'fullBottle'),
(35, 'stain'),
(36, 'elvishTree'),
(37, 'bagpack'),
(38, 'fishSpot'),
(1000, 'any');

-- --------------------------------------------------------

--
-- Table structure for table `potion_types`
--

CREATE TABLE `potion_types` (
  `type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `potion_types`
--

INSERT INTO `potion_types` (`type_id`, `name`) VALUES
(1, 'agility'),
(2, 'strength'),
(3, 'health'),
(4, 'mana'),
(5, 'poison'),
(6, 'death');

-- --------------------------------------------------------

--
-- Table structure for table `races`
--

CREATE TABLE `races` (
  `race_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `races`
--

INSERT INTO `races` (`race_id`, `name`) VALUES
(1, 'human'),
(2, 'elf'),
(3, 'drow'),
(4, 'dwarf'),
(5, 'gnome');

-- --------------------------------------------------------

--
-- Table structure for table `races_attributes`
--

CREATE TABLE `races_attributes` (
  `race_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `races_attributes`
--

INSERT INTO `races_attributes` (`race_id`, `attribute_id`, `value`) VALUES
(1, 1, 1),
(1, 2, 1),
(1, 3, 0),
(1, 4, 0),
(1, 5, 2),
(2, 1, -1),
(2, 2, 3),
(2, 3, 2),
(2, 4, 2),
(2, 5, 1),
(3, 1, 2),
(3, 2, 3),
(3, 3, 2),
(3, 4, 0),
(3, 5, 0),
(4, 1, 3),
(4, 2, 0),
(4, 3, -2),
(4, 4, 0),
(4, 5, 3),
(5, 1, -2),
(5, 2, 3),
(5, 3, 4),
(5, 4, 1),
(5, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `skill_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`skill_id`, `name`) VALUES
(1, 'magic'),
(2, 'steal'),
(3, 'tactics'),
(4, 'weapons'),
(5, 'meditate'),
(6, 'stab'),
(7, 'hide'),
(8, 'survival'),
(9, 'chop'),
(10, 'trade'),
(11, 'defense'),
(12, 'phishing'),
(13, 'minning'),
(14, 'carpentry'),
(15, 'smithing'),
(16, 'leadership'),
(17, 'tame'),
(18, 'projectiles'),
(19, 'wrestling'),
(20, 'navigation');

-- --------------------------------------------------------

--
-- Table structure for table `triggers`
--

CREATE TABLE `triggers` (
  `trigger_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `triggers`
--

INSERT INTO `triggers` (`trigger_id`, `name`) VALUES
(1, 'underRoof'),
(2, 'trigger_2'),
(3, 'invalidPoss'),
(4, 'safeZone'),
(5, 'antiPicket'),
(6, 'fightZone');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `genre_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `race_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_skills`
--

CREATE TABLE `users_skills` (
  `user_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `worlds`
--

CREATE TABLE `worlds` (
  `world_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `tiles_width` int(11) NOT NULL,
  `tiles_height` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `worlds`
--

INSERT INTO `worlds` (`world_id`, `name`, `tiles_width`, `tiles_height`) VALUES
(1, 'Argentum', 3000, 3000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `constants`
--
ALTER TABLE `constants`
  ADD PRIMARY KEY (`constant_id`);

--
-- Indexes for table `objects`
--
ALTER TABLE `objects`
  ADD PRIMARY KEY (`object_id`),
  ADD KEY `objtype_id` (`type_id`);

--
-- Indexes for table `object_attributes`
--
ALTER TABLE `object_attributes`
  ADD KEY `object_id` (`object_id`);

--
-- Indexes for table `object_types`
--
ALTER TABLE `object_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `potion_types`
--
ALTER TABLE `potion_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `races`
--
ALTER TABLE `races`
  ADD PRIMARY KEY (`race_id`);

--
-- Indexes for table `races_attributes`
--
ALTER TABLE `races_attributes`
  ADD KEY `race_id` (`race_id`),
  ADD KEY `attribute_id` (`attribute_id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`skill_id`);

--
-- Indexes for table `triggers`
--
ALTER TABLE `triggers`
  ADD PRIMARY KEY (`trigger_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `race_id` (`race_id`);

--
-- Indexes for table `users_skills`
--
ALTER TABLE `users_skills`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `skill_id` (`skill_id`);

--
-- Indexes for table `worlds`
--
ALTER TABLE `worlds`
  ADD PRIMARY KEY (`world_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `constants`
--
ALTER TABLE `constants`
  MODIFY `constant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `objects`
--
ALTER TABLE `objects`
  MODIFY `object_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `potion_types`
--
ALTER TABLE `potion_types`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `skill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `triggers`
--
ALTER TABLE `triggers`
  MODIFY `trigger_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `worlds`
--
ALTER TABLE `worlds`
  MODIFY `world_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `objects`
--
ALTER TABLE `objects`
  ADD CONSTRAINT `objects_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `object_types` (`type_id`);

--
-- Constraints for table `object_attributes`
--
ALTER TABLE `object_attributes`
  ADD CONSTRAINT `object_attributes_ibfk_1` FOREIGN KEY (`object_id`) REFERENCES `objects` (`object_id`);

--
-- Constraints for table `races_attributes`
--
ALTER TABLE `races_attributes`
  ADD CONSTRAINT `races_attributes_ibfk_1` FOREIGN KEY (`race_id`) REFERENCES `races` (`race_id`),
  ADD CONSTRAINT `races_attributes_ibfk_2` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`attribute_id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`race_id`) REFERENCES `races` (`race_id`);

--
-- Constraints for table `users_skills`
--
ALTER TABLE `users_skills`
  ADD CONSTRAINT `users_skills_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `users_skills_ibfk_2` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`skill_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
