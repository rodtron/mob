-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 14, 2018 at 06:00 PM
-- Server version: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 7.2.7-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mob`
--

-- --------------------------------------------------------

--
-- Table structure for table `npcd_answers`
--

CREATE TABLE `npcd_answers` (
  `answer_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `next_message_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `npcd_answers`
--

INSERT INTO `npcd_answers` (`answer_id`, `message_id`, `text`, `next_message_id`) VALUES
(1, 3, 'Si, es genial.', 4),
(2, 3, 'No, es malísimo.', 5);

-- --------------------------------------------------------

--
-- Table structure for table `npcd_messages`
--

CREATE TABLE `npcd_messages` (
  `message_id` int(11) NOT NULL,
  `conversation_id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `type_id` int(11) NOT NULL,
  `next_message_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `npcd_messages`
--

INSERT INTO `npcd_messages` (`message_id`, `conversation_id`, `text`, `type_id`, `next_message_id`) VALUES
(1, 1, 'Bienvenido a...', 1, 2),
(2, 1, '...un sistema de diálogos.', 1, 3),
(3, 1, '¿Te gusta?', 2, NULL),
(4, 1, '¿Viste? ¡A mí me encanta!', 1, NULL),
(5, 1, 'Uh... bueno... Chau.', 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `npcd_answers`
--
ALTER TABLE `npcd_answers`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `npcd_messages`
--
ALTER TABLE `npcd_messages`
  ADD PRIMARY KEY (`message_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `npcd_answers`
--
ALTER TABLE `npcd_answers`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `npcd_messages`
--
ALTER TABLE `npcd_messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
